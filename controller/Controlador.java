package controller;

import java.util.ArrayList;
import model.Usuario;

public class Controlador {

	ArrayList<Usuario> listausuario;

	public Controlador(){
		
		listausuario = new ArrayList<Usuario>();
		
		public void adicionar(Usuario usuario){
			listausuario.add(usuario);
			System.out.println("Usuario cadastrado com sucesso");
		}
		
		public void remover(Usuario usuario){
			listausuario.remove(usuario);
			System.out.println("Usuario removido com sucesso");
		}
		
		public Usuario pesquisar(String nomeUsuario){
			for (Usuario usuario : listausuario){
				if (usuario.getNome().equalsIgnoreCase(nomeUsuario)){
					return usuario;
				}
			}
			System.out.println("Usuario removido com sucesso");
		}
		
		
	}
}
